using Toybox.WatchUi;
using Toybox.Application;
using Toybox.FitContributor;

class DepthView extends WatchUi.SimpleDataField {

	private var initialPressure = null;
	private var depthField = null;
	private var maxDepthField = null;
	private var maxDepth = 0;
	
	const DEPTH_FIELD_ID = 0;
	const MAX_DEPTH_FIELD_ID = 1;
	// Acceleration of gravity
	const g =  9.80665;
	// Liquid density
    const p = Application.getApp().getProperty("liquid_p");
	
    function initialize() {
        SimpleDataField.initialize();
        label = "Depth";
        depthField = createField(
        	"Depth",
        	DEPTH_FIELD_ID,
        	FitContributor.DATA_TYPE_FLOAT, 
        	{:mesgType=>FitContributor.MESG_TYPE_RECORD, :units=>"m"}
        );
        maxDepthField = createField(
        	"Max Depth",
        	MAX_DEPTH_FIELD_ID,
        	FitContributor.DATA_TYPE_FLOAT, 
        	{:mesgType=>FitContributor.MESG_TYPE_SESSION, :units=>"m"}
        );
    }

    function compute(info) {
    	var pressure = info.rawAmbientPressure;
    	if (pressure == null) {
    		return "N/A";
    	}
    	if(initialPressure == null) {
    		initialPressure = pressure;
    	}
    	var pressureDiff = initialPressure - pressure;
    	var depth = pressureDiff/(g*p);
    	if (depth < 0) {
    		return "N/A";
    	}
    	if (maxDepth < depth) {
    		maxDepth = depth;
    		maxDepthField.setData(maxDepth);
    	}
    	depthField.setData(depth);
        return depth;
    }

}